
# Stock Market App

Stock Market App is a simple Laravel application for displaying stock data with the help of an external API provided by RapidAPI.

## Installation

1. **Clone the repository:**

   ```bash
   git clone https://gitlab.com/darkosakaliev/stock-market-app.git .
   cd stock-market-app
   ```

2. **Install dependencies using Composer:**

   ```bash
   composer install
   ```

3. **Copy the `.env.example` file and rename it to `.env`:**

   ```bash
   cp .env.example .env
   ```

4. **Generate an application key:**

   ```bash
   php artisan key:generate
   ```

5. **Open the `.env` file and add your API key and URL:**

   ```dotenv
   RAPIDAPI_KEY=your_api_key_here
   RAPIDAPI_URL=api_url_here
   ```

## Usage

- **Start the development server:**

  ```bash
  php artisan serve
  ```

- Access the application in your web browser at `http://localhost:8000`.

## Testing

- **Run tests using PHPUnit:**

  ```bash
  php artisan test
  ```
