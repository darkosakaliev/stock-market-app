<nav x-data="{ open: false }" class="bg-white shadow">
    <div class="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      <div class="flex justify-between h-16">
        <div class="flex">
          <div class="flex items-center flex-shrink-0">
            <span>Stock Market</span>
          </div>
          <div class="hidden sm:ml-6 sm:flex sm:space-x-8">
            <x-nav-link :href="route('dashboard')" :active="request()->url() === route('dashboard')">
                Dashboard
            </x-nav-link>
            <x-nav-link :href="route('stock.show', ['US', 'AAPL'])" :active="request()->url() === route('stock.show', ['US', 'AAPL'])">
                AAPL
            </x-nav-link>
            <x-nav-link :href="route('stock.show', ['US', 'AMZN'])" :active="request()->url() === route('stock.show', ['US', 'AMZN'])">
                AMZN
            </x-nav-link>
            <x-nav-link :href="route('stock.show', ['US', 'GOOG'])" :active="request()->url() === route('stock.show', ['US', 'GOOG'])">
                GOOG
            </x-nav-link>
           </div>
        </div>
        <div class="flex items-center -mr-2 sm:hidden">
          <!-- Mobile menu button -->
          <button @click="open = !open" type="button" class="relative inline-flex items-center justify-center p-2 text-gray-400 rounded-md hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-controls="mobile-menu" aria-expanded="false">
            <span class="absolute -inset-0.5"></span>
            <span class="sr-only">Open main menu</span>
            <!--
              Icon when menu is closed.

              Menu open: "hidden", Menu closed: "block"
            -->
            <svg :class="{'block': !open, 'hidden': open}" class="block w-6 h-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
            </svg>
            <!--
              Icon when menu is open.

              Menu open: "block", Menu closed: "hidden"
            -->
            <svg :class="{'block': open, 'hidden': !open}" class="hidden w-6 h-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
            </svg>
          </button>
        </div>
      </div>
    </div>

    <!-- Mobile menu, show/hide based on menu state. -->
    <div x-show="open" class="sm:hidden" id="mobile-menu">
      <div class="pt-2 pb-3 space-y-1">
        <x-responsive-nav-link :href="route('dashboard')" :active="request()->url() === route('dashboard')">
            Dashboard
        </x-responsive-nav-link>
        <x-responsive-nav-link :href="route('stock.show', ['US', 'AAPL'])" :active="request()->url() === route('stock.show', ['US', 'AAPL'])">
            AAPL
        </x-responsive-nav-link>
        <x-responsive-nav-link :href="route('stock.show', ['US', 'AMZN'])" :active="request()->url() === route('stock.show', ['US', 'AMZN'])">
            AMZN
        </x-responsive-nav-link>
        <x-responsive-nav-link :href="route('stock.show', ['US', 'GOOG'])" :active="request()->url() === route('stock.show', ['US', 'GOOG'])">
            GOOG
        </x-responsive-nav-link>
      </div>
    </div>
  </nav>
