@extends('layouts.master')
@section('title', $stockData['symbol'])

@section('content')
    <div x-data="{ loading: true }" x-init="loading = false" class="container p-4 mx-auto">
        <h1 class="mb-4 text-2xl font-bold">Stock Data for {{ $stockData['symbol'] }}</h1>
        <div class="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <div class="flex items-center justify-between p-4 bg-white border rounded-lg shadow hover:border-black">
                <div>
                    <h3 class="text-sm text-black-700">{{ $stockData['symbol'] }}</h3>
                    <div class="flex items-end gap-2">
                        <p class="m-0 text-3xl font-bold text-black">{{ number_format($stockData['regularMarketPreviousClose'], 2) }}</p>
                        <p class="{{ $stockData['priceChange'] >= 0 ? 'text-green-500' : 'text-red-500' }} text-lg font-bold m-0">
                            {{ $stockData['priceChange'] >= 0 ? '+' : '' }}{{ number_format($stockData['priceChange'], 2) }}
                        </p>
                        <p class="{{ $stockData['priceChangePercent'] >= 0 ? 'text-green-500' : 'text-red-500' }} text-lg font-bold m-0">
                            ({{ $stockData['priceChangePercent'] >= 0 ? '+' : '' }}{{ number_format($stockData['priceChangePercent'], 2) }}%)
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container p-4 mx-auto">
        <h1 class="mb-4 text-2xl font-bold">Additional info</h1>
        <div class="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <div class="flex items-center justify-between p-4 bg-white border rounded-lg shadow hover:border-black">
                <div>
                    <p class="text-lg font-bold">Symbol: {{ $stockData['symbol'] }}</p>
                    <p class="text-lg font-bold">Previous Close: {{ $stockData['previousClose'] }}</p>
                    <p class="text-lg font-bold">Regular Market Previous Close: {{ $stockData['regularMarketPreviousClose'] }}</p>
                    <p class="text-lg font-bold">Price Change: {{ $stockData['priceChange'] >= 0 ? '+' : '' }}{{ $stockData['priceChange'] }}</p>
                    <p class="text-lg font-bold">PriceChange in Percentage: {{ $stockData['priceChangePercent'] >= 0 ? '+' : '' }}{{ number_format($stockData['priceChangePercent'], 2) }}</p>
                    <p class="text-lg font-bold">Day High: {{ $stockData['dayHigh'] }}</p>
                    <p class="text-lg font-bold">Day Low: {{ $stockData['dayLow'] }}</p>
                    <p class="text-lg font-bold">Market Cap: {{ $stockData['marketCap'] }}</p>
                    <p class="text-lg font-bold">Volume: {{ $stockData['volume'] }}</p>
                    <p class="text-lg font-bold">Open: {{ $stockData['open'] }}</p>
                    <p class="text-lg font-bold">Ask: {{ $stockData['ask'] }}</p>
                    <p class="text-lg font-bold">Bid: {{ $stockData['bid'] }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
