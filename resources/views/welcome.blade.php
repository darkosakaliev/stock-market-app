@extends('layouts.master')
@section('title', 'Dashboard')

@section('content')
    <div class="container p-4 mx-auto">
        <h1 class="mb-4 text-4xl font-bold text-center">Welcome to the Stock Market App</h1>
        <p class="mb-4 text-center">Explore and track the performance of various company stocks.</p>
        <div class="flex flex-col items-center space-y-2 sm:space-y-0 sm:flex-row sm:justify-center sm:space-x-4">
            <a href="{{ route('stock.show', ['US', 'AAPL']) }}" class="w-full px-4 py-2 font-semibold text-center text-white bg-blue-500 rounded-full sm:w-auto hover:bg-blue-600">
                View AAPL
            </a>
            <a href="{{ route('stock.show', ['US', 'AMZN']) }}" class="w-full px-4 py-2 font-semibold text-center text-white bg-green-500 rounded-full sm:w-auto hover:bg-green-600">
                View AMZN
            </a>
            <a href="{{ route('stock.show', ['US', 'GOOG']) }}" class="w-full px-4 py-2 font-semibold text-center text-white bg-red-500 rounded-full sm:w-auto hover:bg-red-600">
                View GOOG
            </a>
        </div>
    </div>
@endsection
