<?php

namespace Tests\Unit;

use App\Services\StockService;
use Tests\TestCase;

class StockServiceTest extends TestCase
{
    public function testGetStockSummary()
    {
        $mockService = $this->getMockBuilder(StockService::class)
                            ->disableOriginalConstructor()
                            ->getMock();

        $mockService->method('getStockSummary')
                    ->willReturn([
                        'previousClose' => 100.00,
                        'regularMarketPreviousClose' => 101.00,
                        'ask' => 102.00,
                        'regularMarketDayHigh' => 103.00,
                        'regularMarketDayLow' => 99.00,
                        'marketCap' => 2000000000,
                        'volume' => 1000000,
                        'regularMarketOpen' => 105.00,
                        'bid' => 104.00,
                    ]);

        $data = $mockService->getStockSummary('US', 'AAPL');

        $this->assertIsArray($data);
        $this->assertArrayHasKey('previousClose', $data);
        $this->assertArrayHasKey('regularMarketPreviousClose', $data);
        $this->assertArrayHasKey('ask', $data);
        $this->assertArrayHasKey('regularMarketDayHigh', $data);
        $this->assertArrayHasKey('regularMarketDayLow', $data);
        $this->assertArrayHasKey('marketCap', $data);
        $this->assertArrayHasKey('volume', $data);
        $this->assertArrayHasKey('regularMarketOpen', $data);
        $this->assertArrayHasKey('bid', $data);
    }
}
