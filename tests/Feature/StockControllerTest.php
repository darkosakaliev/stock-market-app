<?php

namespace Tests\Feature;

use App\Services\StockService;
use Tests\TestCase;

class StockControllerTest extends TestCase
{
    /**
     * Mock StockService instance.
     *
     * @var StockService|\PHPUnit\Framework\MockObject\MockObject
     */
    private $stockServiceMock;

    /**
     * Setup before each test.
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Create a mock instance of StockService
        $this->stockServiceMock = $this->createMock(StockService::class);
    }

    public function testShow()
    {
        // Define expected data from the mock
        $expectedData = [
            'symbol' => 'AAPL',
            'previousClose' => 100.00,
            'regularMarketPreviousClose' => 101.00,
            'ask' => 102.00,
            'dayHigh' => 103.00,
            'dayLow' => 99.00,
            'marketCap' => 2000000000,
            'volume' => 1000000,
            'open' => 105.00,
            'bid' => 104.00,
            'priceChange' => number_format(101.00 - 102.00, 2),
            'priceChangePercent' => number_format((102.00 - 101.00) / 101.00 * 100, 2),
        ];

        // Set the mock expectation for getStockSummary
        $this->stockServiceMock->expects($this->once())
            ->method('getStockSummary')
            ->with('US', 'AAPL')
            ->willReturn($expectedData);

        // Replace the actual instance with the mock
        $this->app->instance(StockService::class, $this->stockServiceMock);

        // Make the HTTP request to your endpoint
        $response = $this->get('/stock/US/AAPL');

        // Assert the response status and view data
        $response->assertStatus(200);
        $response->assertViewHas('stockData', $expectedData);
    }
}
