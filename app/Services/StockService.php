<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Http;

class StockService
{
    protected $apiUrl;
    protected $apiKey;

    public function __construct()
    {
        $this->apiUrl = env('RAPIDAPI_URL');
        $this->apiKey = env('RAPIDAPI_KEY');
    }

    public function getStockSummary($region, $symbol)
    {
        // Check if apiUrl or apiKey are not set
        if (empty($this->apiUrl) || empty($this->apiKey)) {
            throw new Exception('API configuration is missing.');
        }

        try {
            $response = Http::withHeaders([
                'X-RapidAPI-Key' => $this->apiKey,
            ])->get("https://{$this->apiUrl}/stock/get_summary", [
                'region' => $region,
                'symbol' => $symbol,
            ]);

            $data = $response->json($symbol);

            return [
                'symbol' => $symbol,
                'previousClose' => number_format($data['previousClose'], 2),
                'regularMarketPreviousClose' => number_format($data['regularMarketPreviousClose'], 2),
                'priceChange' => number_format($data['regularMarketPreviousClose'] - $data['ask'], 2),
                'priceChangePercent' => ($data['ask'] - $data['regularMarketPreviousClose']) / $data['regularMarketPreviousClose'] * 100,
                'dayHigh' => number_format($data['regularMarketDayHigh'], 2),
                'dayLow' => number_format($data['regularMarketDayLow'], 2),
                'marketCap' => number_format($data['marketCap'], 2),
                'volume' => number_format($data['volume'], 2),
                'open' => number_format($data['regularMarketOpen'], 2),
                'ask' => number_format($data['ask'], 2),
                'bid' => number_format($data['bid'], 2),
            ];
        } catch (Exception $e) {
            throw new Exception('Failed to fetch stock data: ' . $e->getMessage());
        }
    }
}
