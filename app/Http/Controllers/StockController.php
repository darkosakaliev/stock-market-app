<?php

namespace App\Http\Controllers;

use App\Services\StockService;
use Illuminate\Http\Request;

class StockController extends Controller
{
    protected $stockService;

    public function __construct(StockService $stockService)
    {
        $this->stockService = $stockService;
    }

    public function show($region, $symbol)
    {
        $stockData = $this->stockService->getStockSummary($region, $symbol);

        return view('stock.show', compact('stockData'));
    }
}
